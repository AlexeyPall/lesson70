
import numpy as np
from scipy.cluster.vq import vq, kmeans, whiten
import matplotlib.pyplot as plt
import os




# features  = np.array( [ [ 1.9, 2.3],
#                         [ 1.5, 2.5],
#                         [ 0.8, 0.6],
#                         [ 0.4, 1.8],
#                         [ 0.2, 1.8],
#                         [ 2.0, 0.5],
#                         [ 0.1, 0.1],
#                         [ 0.3, 1.5],
#                         [ 1.0, 1.0]])
#
#
#
# res, distortion = kmeans(features, 3)

# print(res)
# print(distortion)

def dist(v1, v2):

    sum = 0

    for i in range(len(v1)):
        sum += (v1[i] - v2[i])**2

    return np.sqrt(sum)

def read_file(filename):

    data = []

    for _line in open(filename).readlines():
        if len(_line.strip()) > 0:
            line_data = []
            for _data in _line.split():
                line_data.append(float(_data))

            data.append(line_data)

    return np.array(data)


# plt.plot(res[:, 0], res[:, 1], 'r*', markersize=20)
# plt.plot(features[:, 0], features[:, 1], 'b.')

PATH_TO_SAMPLES = 'data/clusters_samples'

possible = ['2dSampleLsun.txt']
            # '2dSampleWingNut.txt',
            # '2dSampleElongate.txt']

for _file in os.listdir(PATH_TO_SAMPLES):
    if _file.startswith('2d') and _file in possible:
        data = read_file(PATH_TO_SAMPLES + '/' + _file)

        res, distortion = kmeans(data, 3)

        print(_file)
        plt.figure()

        print(res[0, :])
        print(data[0, :])
        print(dist(res[0, :], data[0, :]))

        for _data_idx in range(len(data)):
            if dist(res[0, :], data[_data_idx, :]) < dist(res[1, :], data[_data_idx, :]) \
               and dist(res[0, :], data[_data_idx, :]) < dist(res[2, :], data[_data_idx, :]):
                plt.plot(data[_data_idx, 0], data[_data_idx, 1], 'b.')
            elif dist(res[1, :], data[_data_idx, :]) < dist(res[2, :], data[_data_idx, :]):
                plt.plot(data[_data_idx, 0], data[_data_idx, 1], 'r.')
            else:
                plt.plot(data[_data_idx, 0], data[_data_idx, 1], 'g.')

        plt.plot(res[0, 0], res[0, 1], 'm*', markersize=30)
        plt.plot(res[1, 0], res[1, 1], 'k*', markersize=30)
        plt.plot(res[2, 0], res[2, 1], 'y*', markersize=30)
        plt.show()

