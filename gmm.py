
import numpy as np


LOG_2_PI = np.log(2*np.pi)


class DiagGauss:

    def __computeGconst(self):
        self.__assertDimsOk()
        self.__Gconst=np.empty_like(self.__weights)

        for mix in range(self.__numMix):
            '''mozhno vektorizovat', no vrode ne nuzhno'''
            self.__Gconst[mix] = \
                np.log(self.__weights[mix]) \
                - 0.5 * self.__dim * LOG_2_PI \
                + 0.5 * np.sum(np.log(self.__invvars[mix])) \
                - 0.5 * np.sum(self.__means[mix] * self.__means[mix] * self.__invvars[mix])

    def __assertDimsOk(self):

        assert self.__means.shape == self.__invvars.shape
        assert self.__invvars.shape == (self.__numMix, self.__dim)
        assert self.__weights.shape == (self.__numMix, )

    def __init__(self, weights, means, vars):
        """ train function """

        self.__weights = weights
        self.__means = means
        self.__invvars = np.power(vars, -1)

        self.__numMix = self.__weights.shape[0]
        self.__dim = self.__means.shape[1]

        self.__assertDimsOk()

        self.__computeGconst()

    def loglikelyhood(self, x):
        """ x can be a matrix """

        x = np.array(x)

        self.__assertDimsOk()
        assert x.shape[-1] == self.__dim, \
            "dim must be " + str(self.__means.shape) + " , but shape of x is " + str(x)

        sqr_x = x * x

        '''vektorizovat' bilo bi neploho'''
        lh = 0
        for mix in range(self.__numMix):
            ''' kaldi use means_invvar '''
            lh += self.__Gconst[mix] \
                - 0.5 * np.dot(sqr_x, self.__invvars[mix]) \
                + np.dot(x, self.__means[mix] * self.__invvars[mix])

        return lh


if __name__== "__main__":

    print("small test")
    import matplotlib.pyplot as plt
    import math
    import scipy.stats

    n = 1000000
    dim = 2
    mean = np.array([[0,0]])
    var = np.array([[1,1]])
    weights = np.array([1])
    gauss = DiagGauss(weights,mean,var)
    X = np.random.random((n,2))*20-10
    Yr = np.random.random((n,))
    Yn = np.exp(gauss.loglikelyhood(X))
    fOK = (Yr <= Yn) #ne uveren, chto eto legalno
    toPlot = X[fOK, :]

    ################################################################
    #krasiviye kruzhochki
    for component in range(weights.shape[0]):
        plt.plot(mean[component, 0], mean[component, 1], 'r*')

        if np.isclose(var[component, 0], var[component, 1]):
            O = plt.Circle(mean[component], math.sqrt(var[component, 0]), color="r", fill=False)
            plt.gcf().gca().add_artist(O)

            O = plt.Circle(mean[component], 2 * math.sqrt(var[component, 0]), color="g", fill=False)
            plt.gcf().gca().add_artist(O)

            O = plt.Circle(mean[component], 3 * math.sqrt(var[component, 0]), color="y", fill=False)
            plt.gcf().gca().add_artist(O)

    ################################################################
    k = 2500
    plt.plot(toPlot[:k, 0], toPlot[:k, 1], '.')
    plt.show(True)
