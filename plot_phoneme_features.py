
import matplotlib.pyplot as plt
import numpy as np


PH_FILE_NAME  = '../ASRlesson060/data/VoxForge/sort_dir/aa'
PH_FILE_NAME2 = '../ASRlesson060/data/VoxForge/sort_dir/c'


def read_phoneme_to_array(file_name):
    """ """
    with open(file_name) as f:
        ph_vectors = []
        for _ph in f.readlines():
            ph_vectors.append(_ph.split())

    return np.array(ph_vectors, dtype=float)


def plot_by_2d_coords(ph_v1, ph_v2):

    for i in range(0, 12, 2):
        plt.plot(ph_v1[:, i], ph_v1[:, i + 1], 'o')
        plt.plot(ph_v2[:, i], ph_v2[:, i + 1], 'ro')
        plt.show()

    plt.plot(ph_v1[:, 11], ph_v1[:, 12], 'o')
    plt.plot(ph_v2[:, 11], ph_v2[:, 12], 'ro')
    plt.show()

if __name__ == '__main__':

    ph_vectors = read_phoneme_to_array(PH_FILE_NAME)
    ph_vectors2 = read_phoneme_to_array(PH_FILE_NAME2)

    plot_by_2d_coords(ph_vectors, ph_vectors2)

